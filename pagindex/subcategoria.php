<?php
require_once '../modelo/categoria.entidad.php';
require_once '../modelo/categoria.model.php';
require_once '../modelo/subcategoria.entidad.php';
require_once '../modelo/subcategoria.model.php';
// Logica de negocio
$alm = new Subcategoria();
$model = new SubcategoriaModel();
if(isset($_REQUEST['action']))
{
switch($_REQUEST['action'])
{
case 'actualizar':
$alm->__SET('idsubcategoria', $_REQUEST['idsubcategoria']);
$alm->__SET('descripcion_subcat', $_REQUEST['descripcion_subcat']);
$alm->__SET('categoria_idcategoria',
$_REQUEST['categoria_idcategoria']);
$model->Actualizar($alm);
header('Location: subcategoria.php');
break;
case 'registrar':
$alm->__SET('descripcion_subcat', $_REQUEST['descripcion_subcat']);
$alm->__SET('categoria_idcategoria', $_REQUEST['categoria_idcategoria']);
$model->Registrar($alm);
header('Location: subcategoria.php');
break;
case 'eliminar':
$model->Eliminar($_REQUEST['idsubcategoria']);
header('Location: subcategoria.php');
break;
case 'editar':
$alm = $model->Obtener($_REQUEST['idsubcategoria']);
break;
}
}
?>

<!DOCTYPE html>
<html lang="es">
<head>
 <h1>FORMULARIO DE ENTRADA...</h1><h1>Subcategorias</h1><br><br>
<title>Anexsoft</title>
 <link rel="stylesheet" href="http://yui.yahooapis.com/pure/0.5.0/pure-min.css">
</head>
 <body style="padding:15px;">
 <div class="pure-g">
 <div class="pure-u-1-12">

 <form action="?action=<?php echo $alm->idsubcategoria > 0 ? 'actualizar' : 'registrar'; ?>" method="POST" class="pure-form pure-formstacked"
style="margin-bottom:30px;">
 <input type="hidden" name="idsubcategoria" value="<?php echo $alm->__GET('idsubcategoria'); ?>" />

 <table style="width:500px;">
 <tr>
 <th style="text-align:left;">Subcategoria</th>
 <td><input type="text" name="descripcion_subcat" placeholder="Nombre subcategoria" required="" value="<?php echo 
 $alm->__GET('descripcion_subcat'); ?>" style="width:100%;" /></td>
 </tr>
 <tr>
    <th style="text-align:left;">Categoria</th>
    <td>
        <select name="categoria_idcategoria" style="width:100%;">
            <option value="0">--Seleccione--</option>
                <?php
                $tp = new CategoriaModel();
                foreach($tp->Listar() as $t):
                ?>
                <option value="<?php echo $t->__GET('idcategoria') ?>"
                                   <?php echo $t->__GET('idcategoria') == $alm->__GET('categoria_idcategoria') ? 'selected' : ''?>>
                                    <?php echo $t->__GET('descripcion_categoria') ?></option>
                <?php endforeach; ?>
                </select>
    </td>
 </tr>
 <tr>
 <td colspan="2">
 <button type="submit" class="pure-button pure-button-primary">Guardar</button>
 </td>
 </tr>
 </table>
 </form>
 <table class="pure-table pure-table-horizontal">
 <thead>
 <tr>
 <th style="text-align:left;">Descripcion subCategoria</th>
 <th style="text-align:left;">Categoria</th>
 <th></th>
 <th></th>
 </tr>
 </thead>
 <?php foreach($model->Listar() as $r): ?>
 <tr>
 <td><?php echo $r->__GET('descripcion_subcat'); ?></td>
 <td><?php echo $r->__GET('descripcion_categoria'); ?></td>

 <td>
 <a href="?action=editar&idsubcategoria=<?php echo $r->idsubcategoria; ?>">Editar</a>
 </td>
 <td>
 <a href="?action=eliminar&idsubcategoria=<?php echo $r->idsubcategoria; ?>">Eliminar</a>
 </td>
 </tr>
 <?php endforeach; ?>
 </table>

 </div>
 </div>
 </body>
</html>