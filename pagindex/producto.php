<?php
require_once '../modelo/producto.entidad.php';
require_once '../modelo/producto.model.php';
require_once '../modelo/proveedor.entidad.php';
require_once '../modelo/proveedor.model.php';
require_once '../modelo/subcategoria.entidad.php';
require_once '../modelo/subcategoria.model.php';
// Logica de negocio
$alm = new Producto();
$model = new ProductoModel();
if(isset($_REQUEST['action']))
{
switch($_REQUEST['action'])
{
case 'actualizar':
$alm->__SET('idproducto',                   $_REQUEST['idproducto']);
$alm->__SET('cod_producto',               $_REQUEST['cod_producto']);
$alm->__SET('nombre_producto',            $_REQUEST['nombre_producto']);
$alm->__SET('color',                $_REQUEST['color']);
$alm->__SET('medida',            $_REQUEST['medida']);
$alm->__SET('precio',                    $_REQUEST['precio']);
$alm->__SET('stock',                      $_REQUEST['stock']);
$alm->__SET('idsubcategoria',                   $_REQUEST[' idsubcategoria']);
$alm->__SET('idproveedor',                   $_REQUEST['idproveedor']);
$model->Actualizar($alm);
header('Location: producto.php');
break;
case 'registrar':
$alm->__SET('nombre_producto',             $_REQUEST['nombre_producto']);
$alm->__SET('cod_producto',                $_REQUEST['cod_producto']);
$alm->__SET('color',                 $_REQUEST['color']);
$alm->__SET('medida',             $_REQUEST['medida']);
$alm->__SET('precio',                     $_REQUEST['precio']);
$alm->__SET('stock',                       $_REQUEST['stock']);
$alm->__SET('idsubcategoria',                    $_REQUEST['idsubcategoria']);
$alm->__SET('idproveedor',                     $_REQUEST['idproveedor']);
 $model->Registrar($alm);
header('Location: producto.php');
break;
case 'eliminar':
$model->Eliminar($_REQUEST['idproducto']);
header('Location: producto.php');
break;
case 'editar':
$alm = $model->Obtener($_REQUEST['idproducto']);
break;
}
}
?>
<!DOCTYPE html>
<html lang="es">
<head>
 <h1>FORMULARIO DE ENTRADA...</h1><h1>producto</h1><br><br>
<title>Anexsoft</title>
 <link rel="stylesheet" href="http://yui.yahooapis.com/pure/0.5.0/pure-min.css">
</head>
 <body style="padding:15px;">

<div class="pure-g">
 <div class="pure-u-1-12">

 <form action="?action=<?php echo $alm->idproducto > 0 ? 'actualizar' : 'registrar'; ?>" method="post" class="pure-form pure-formstacked"
style="margin-bottom:30px;">
 <input type="hidden" name="idproducto" value="<?php echo $alm->__GET('idproducto'); ?>" />

 <table style="width:500px;">
<tr>
 <th style="text-align:left;">cod produto</th>
 <td><input type="text" name="cod_producto" placeholder=" cod_producto" required="" value="<?php echo 
 $alm->__GET('cod_producto'); ?>" style="width:100%;" /></td>
 </tr>
 <tr>
 <th style="text-align:left;">nombre_producto</th>
 <td><input type="text" name="nombre_producto" placeholder=" Documento Cliente" required="" value="<?php echo 
 $alm->__GET('nombre_producto'); ?>" style="width:100%;" /></td>
 </tr>
 <tr>
 <th style="text-align:left;">color</th>
 <td><input type="text" name="color" placeholder=" Razon Social" required="" value="<?php echo 
 $alm->__GET('color'); ?>" style="width:100%;" /></td>
 </tr>
 <tr>
 <th style="text-align:left;">medida</th>
 <td><input type="text" name="medida" placeholder=" Contacto" required="" value="<?php echo 
 $alm->__GET('medida'); ?>" style="width:100%;" /></td>
 </tr>
 <tr>
 <th style="text-align:left;">precio</th>
 <td><input type="number" name="precio" placeholder=" precio Cliente" required="" value="<?php echo 
 $alm->__GET('precio'); ?>" style="width:100%;" /></td>
 </tr>
 <tr>
 <th style="text-align:left;">stock</th>
 <td><input type="text" placeholder="stock" required="" name=" stock" value="<?php echo 
 $alm->__GET('stock'); ?>" style="width:100%;" /></td>
 </tr>
 <tr>
    <th style="text-align:left;">subcategoria</th>
    <td>
        <select name="idsubcategoria" style="width:100%;">
            <option value="0">--Seleccione--</option>
                <?php
                $mo = new SubcategoriaModel();
                foreach($mo->Listar() as $t):
                ?>
                <option value="<?php echo $t->__GET('idsubcategoria') ?>"
                <?php echo $t->__GET('idsubcategoria') == $alm->__GET(
                'idsubcategoria') ? 'selected' : ''?>>
                <?php echo $t->__GET('descripcion_subcat') ?></option>
                <?php endforeach; ?>
                </select>
    </td>
 </tr>
 <tr>
    <th style="text-align:left;">proveedor</th>
    <td>
        <select name="idproveedor" style="width:100%;">
            <option value="0">--Seleccione--</option>
                <?php
                $mo = new ProveedorModel();
                foreach($mo->Listar() as $m):
                ?>
                <option value="<?php echo $m->__GET('idproveedor') ?>"  
                <?php echo $m->__GET('idproveedor') == $alm->__GET(
                'idproveedor') ? 'selected' : ''?>>
                <?php echo $m->__GET('razon_social') ?></option>
                <?php endforeach; ?>
                </select>
    </td>
 </tr>
 <tr>
 <td colspan="2">
 <button type="submit" class="pure-button pure-button-primary">Guardar</button>
 </td>
 </tr>
 </table>
 </form>
 <table class="pure-table pure-table-horizontal">
 <thead>
 <tr>
 <th style="text-align:left;">cod_producto</th>
 <th style="text-align:left;">nombre_producto</th> 
 <th style="text-align:left;">color</th>
 <th style="text-align:left;">medida</th>
 <th style="text-align:left;">precio</th>
 <th style="text-align:left;">stock</th>
 <th style="text-align:left;"> subcategoria</th>
 <th style="text-align:left;">idproveedor</th>
 <th></th>
 <th></th>
 </tr>
 </thead>
 <?php foreach($model->Listar() as $r): ?>
 <tr>
  <td><?php echo $r->__GET('cod_producto'); ?></td>
 <td><?php echo $r->__GET('nombre_producto'); ?></td>
 <td><?php echo $r->__GET('color'); ?></td>
 <td><?php echo $r->__GET('medida'); ?></td>
 <td><?php echo $r->__GET('precio'); ?></td>
 <td><?php echo $r->__GET('stock'); ?></td>
 <td><?php echo $r->__GET('idsubcategoria'); ?></td>
 <td><?php echo $r->__GET('idproveedor'); ?></td>
 <td>
 <a href="?action=editar&idproducto=<?php echo $r->idproducto; ?>">Editar</a>
 </td>
 <td>
 <a href="?action=eliminar&idproducto=<?php echo $r->idproducto; ?>">Eliminar</a>
 </td>
 </tr>
 <?php endforeach; ?>
 </table>

 </div>
 </div>

</div> <!-- fin editable -->



  <hr>
    <footer> <!-- inicio pie de pagina -->
        <div class="container"> 
          <div calss="row">
            <div class="col-xs-12">
                <div class="text-center">
            <font  class="pull-center" size="6" style="color: white">Elaborado por: <font style="color: red;"> 4SW</font>   Encuentranos en: </font>           
               <a  href="https://twitter.com/"> <img src="imagenes/twitter.png" width="50"></a >
               <a href="https://www.facebook.com/"> <img src="imagenes/facebook.png" width="50"></a >
               <a href="https://www.instagram.com/"> <img src="imagenes/instagram.png" width="50"></a >
               </div>
             </div>
         </div>      
        </div>
    </footer> <!-- fin pie de pagina -->


    <!-- jQuery -->
    <script src="bootstrap/js/jquery.min.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="bootstrap/js/bootstrap.min.js"></script>

    <!-- Metis Menu Plugin JavaScript -->
    <script src="bootstrap/css/metisMenu.min.js"></script>

    <script src=".script/bootstrap.min.js"></script>
    <!-- Metis Menu Plugin JavaScript -->
    <script src=".script/jquery.metisMenu.js"></script>
    <!-- DataTables JavaScript -->
    <script src="script/jquery.dataTables.js"></script>
    <script src="script/dataTables.bootstrap.js"></script>
    <!-- Morris Charts JavaScript -->
    <script src="script/raphael-2.1.0.min.js"></script>
    <script src="script/morris.js"></script>
    <!-- Morris Charts JavaScript -->
    <script src="script/Chart.js"></script>
    <script src="script/custom.js"></script>
    <script>
        $(document).ready(function () {
            $('#dataTables-example').dataTable();
        });
    </script>

    <script language="JavaScript"> 
        function mueveReloj(){ 
            momentoActual = new Date() 
            hora = momentoActual.getHours() 
            minuto = momentoActual.getMinutes() 
            segundo = momentoActual.getSeconds() 

            str_segundo = new String (segundo) 
            if (str_segundo.length == 1) 
                segundo = "0" + segundo 

            str_minuto = new String (minuto) 
            if (str_minuto.length == 1) 
                minuto = "0" + minuto 

            str_hora = new String (hora) 
            if (str_hora.length == 1) 
                hora = "0" + hora 

            horaImprimible = hora + " : " + minuto + " : " + segundo 

            document.form_reloj.reloj.value = horaImprimible 

            setTimeout("mueveReloj()",1000) 
        } 
    </script> 

    <!-- Morris Charts JavaScript -->
   <!--   <script src="../vendor/raphael/raphael.min.js"></script>
    <script src="../vendor/morrisjs/morris.min.js"></script>
    <script src="../data/morris-data.js"></script>
    <script src="./SisControl 3.0_files/jquery.metisMenu.js.descarga"></script> -->

    <!-- Custom Theme JavaScript -->
    <script src="bootstrap/js/sb-admin-2.js"></script>


    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->


<script type="text/javascript">
    $(document).ready(function(){
        $('#test_modal').modal('show');
    });
</script>

</body>


</html>
