<?php
require_once '../modelo/usuario.entidad.php';
require_once '../modelo/usuario.model.php';
require_once '../modelo/genero.entidad.php';
require_once '../modelo/genero.model.php';
require_once '../modelo/tipoDocumento.entidad.php';
require_once '../modelo/tipoDocumento.model.php';
require_once '../modelo/cargo.entidad.php';
require_once '../modelo/cargo.model.php';
// Logica de negocio
$alm = new Usuario();
$model = new UsuarioModel();
if(isset($_REQUEST['action']))
{
switch($_REQUEST['action'])
{
case 'actualizar':
$alm->__SET('idusuario',                   $_REQUEST['idusuario']);
$alm->__SET('idgenero',               $_REQUEST['idgenero']);
$alm->__SET('idtipo_documento',            $_REQUEST['idtipo_documento']);
$alm->__SET('numero_documento',                $_REQUEST['numero_documento']);
$alm->__SET('nombre_apellido',            $_REQUEST['nombre_apellido']);
$alm->__SET('usuario',                    $_REQUEST['usuario']);
$alm->__SET('idcargo',                   $_REQUEST['idcargo']);
$model->Actualizar($alm);
header('Location: usuario.php');
break;
case 'registrar':
$alm->__SET('idtipo_documento',             $_REQUEST['idtipo_documento']);
$alm->__SET('idgenero',                $_REQUEST['idgenero']);
$alm->__SET('numero_documento',                 $_REQUEST['numero_documento']);
$alm->__SET('nombre_apellido',             $_REQUEST['nombre_apellido']);
$alm->__SET('usuario',                     $_REQUEST['usuario']);
$alm->__SET('idcargo',                     $_REQUEST['idcargo']);
 $model->Registrar($alm);
header('Location: usuario.php');
break;
case 'eliminar':
$model->Eliminar($_REQUEST['idusuario']);
header('Location: usuario.php');
break;
case 'editar':
$alm = $model->Obtener($_REQUEST['idusuario']);
break;
}
}
?>
<!DOCTYPE html>
<html lang="es">
<head>
 <h1>FORMULARIO DE ENTRADA...</h1><h1>usuario</h1><br><br>
<title>Anexsoft</title>
 <link rel="stylesheet" href="http://yui.yahooapis.com/pure/0.5.0/pure-min.css">
</head>
 <body style="padding:15px;">

<div class="pure-g">
 <div class="pure-u-1-12">

 <form action="?action=<?php echo $alm->idusuario > 0 ? 'actualizar' : 'registrar'; ?>" method="post" class="pure-form pure-formstacked"
style="margin-bottom:30px;">
 <input type="hidden" name="idusuario" value="<?php echo $alm->__GET('idusuario'); ?>" />

 <table style="width:500px;">
 
  <tr>
    <th style="text-align:left;">Genero</th>
    <td>
        <select name="idgenero" style="width:100%;">
            <option value="0">--Seleccione--</option>
                <?php
                $mo = new GeneroModel();
                foreach($mo->Listar() as $m):
                ?>
                <option value="<?php echo $m->__GET('idgenero') ?>"
                <?php echo $m->__GET('idgenero') == $alm->__GET(
                'idgenero') ? 'selected' : ''?>>
                <?php echo $m->__GET('descripciong') ?></option>
                <?php endforeach; ?>
                </select>
    </td>
 </tr>
  <tr>
    <th style="text-align:left;">tipo documento</th>
    <td>
        <select name="idtipo_documento" style="width:100%;">
            <option value="0">--Seleccione--</option>
                <?php
                $mo = new TipoDocumentoModel();
                foreach($mo->Listar() as $m):
                ?>
                <option value="<?php echo $m->__GET('idtipo_documento') ?>"
                <?php echo $m->__GET('idtipo_documento') == $alm->__GET(
                'idtipo_documento') ? 'selected' : ''?>>
                <?php echo $m->__GET('descripcion') ?></option>
                <?php endforeach; ?>
                </select>
    </td>
 </tr>
 <th style="text-align:left;">numero_documento</th>
 <td><input type="text" name="numero_documento" placeholder=" numero_documento" required="" value="<?php echo 
 $alm->__GET('numero_documento'); ?>" style="width:100%;" /></td>
 </tr>
 <tr>
 <th style="text-align:left;">nombre_apellido</th>
 <td><input type="text" name="nombre_apellido" placeholder=" nombre_apellido" required="" value="<?php echo 
 $alm->__GET('nombre_apellido'); ?>" style="width:100%;" /></td>
 </tr>
 <tr>
 <th style="text-align:left;">usuario</th>
 <td><input type="text" name="usuario" placeholder=" usuario" required="" value="<?php echo 
 $alm->__GET('usuario'); ?>" style="width:100%;" /></td>
 </tr>

  <tr>
    <th style="text-align:left;">cargo</th>
    <td>
        <select name="idcargo" style="width:100%;">
            <option value="0">--Seleccione--</option>
                <?php
                $mo = new CargoModel();
                foreach($mo->Listar() as $m):
                ?>
                <option value="<?php echo $m->__GET('idcargo') ?>"
                <?php echo $m->__GET('idcargo') == $alm->__GET(
                'idcargo') ? 'selected' : ''?>>
                <?php echo $m->__GET('descripcion_cargo') ?></option>
                <?php endforeach; ?>
                </select>
    </td>
 </tr>

 <tr>
 <td colspan="2">
 <button type="submit" class="pure-button pure-button-primary">Guardar</button>
 </td>
 </tr>
 </table>
 </form>
 <table class="pure-table pure-table-horizontal">
 <thead>
 <tr>
 <th style="text-align:left;">idgenero</th>
 <th style="text-align:left;">tipo documento</th> 
 <th style="text-align:left;">numero_documento</th>
 <th style="text-align:left;">nombre_apellido</th>
 <th style="text-align:left;">usuario</th>
 <th style="text-align:left;">cargo</th>

 <th></th>
 <th></th>
 </tr>
 </thead>
 <?php foreach($model->Listar() as $r): ?>
 <tr>
  <td><?php echo $r->__GET('idgenero'); ?></td>
 <td><?php echo $r->__GET('idtipo_documento'); ?></td>
 <td><?php echo $r->__GET('numero_documento'); ?></td>
 <td><?php echo $r->__GET('nombre_apellido'); ?></td>
 <td><?php echo $r->__GET('usuario'); ?></td>
 <td><?php echo $r->__GET('idcargo'); ?></td>
 <td>
 <a href="?action=editar&idusuario=<?php echo $r->idusuario; ?>">Editar</a>
 </td>
 <td>
 <a href="?action=eliminar&idusuario=<?php echo $r->idusuario; ?>">Eliminar</a>
 </td>
 </tr>
 <?php endforeach; ?>
 </table>

 </div>
 </div>

</div> <!-- fin editable -->



  <hr>
    <footer> <!-- inicio pie de pagina -->
        <div class="container"> 
          <div calss="row">
            <div class="col-xs-12">
                <div class="text-center">
            <font  class="pull-center" size="6" style="color: white">Elaborado por: <font style="color: red;"> 4SW</font>   Encuentranos en: </font>           
               <a  href="https://twitter.com/"> <img src="imagenes/twitter.png" width="50"></a >
               <a href="https://www.facebook.com/"> <img src="imagenes/facebook.png" width="50"></a >
               <a href="https://www.instagram.com/"> <img src="imagenes/instagram.png" width="50"></a >
               </div>
             </div>
         </div>      
        </div>
    </footer> <!-- fin pie de pagina -->


    <!-- jQuery -->
    <script src="bootstrap/js/jquery.min.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="bootstrap/js/bootstrap.min.js"></script>

    <!-- Metis Menu Plugin JavaScript -->
    <script src="bootstrap/css/metisMenu.min.js"></script>

    <script src=".script/bootstrap.min.js"></script>
    <!-- Metis Menu Plugin JavaScript -->
    <script src=".script/jquery.metisMenu.js"></script>
    <!-- DataTables JavaScript -->
    <script src="script/jquery.dataTables.js"></script>
    <script src="script/dataTables.bootstrap.js"></script>
    <!-- Morris Charts JavaScript -->
    <script src="script/raphael-2.1.0.min.js"></script>
    <script src="script/morris.js"></script>
    <!-- Morris Charts JavaScript -->
    <script src="script/Chart.js"></script>
    <script src="script/custom.js"></script>
    <script>
        $(document).ready(function () {
            $('#dataTables-example').dataTable();
        });
    </script>

    <script language="JavaScript"> 
        function mueveReloj(){ 
            momentoActual = new Date() 
            hora = momentoActual.getHours() 
            minuto = momentoActual.getMinutes() 
            segundo = momentoActual.getSeconds() 

            str_segundo = new String (segundo) 
            if (str_segundo.length == 1) 
                segundo = "0" + segundo 

            str_minuto = new String (minuto) 
            if (str_minuto.length == 1) 
                minuto = "0" + minuto 

            str_hora = new String (hora) 
            if (str_hora.length == 1) 
                hora = "0" + hora 

            horaImprimible = hora + " : " + minuto + " : " + segundo 

            document.form_reloj.reloj.value = horaImprimible 

            setTimeout("mueveReloj()",1000) 
        } 
    </script> 

    <!-- Morris Charts JavaScript -->
   <!--   <script src="../vendor/raphael/raphael.min.js"></script>
    <script src="../vendor/morrisjs/morris.min.js"></script>
    <script src="../data/morris-data.js"></script>
    <script src="./SisControl 3.0_files/jquery.metisMenu.js.descarga"></script> -->

    <!-- Custom Theme JavaScript -->
    <script src="bootstrap/js/sb-admin-2.js"></script>


    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->


<script type="text/javascript">
    $(document).ready(function(){
        $('#test_modal').modal('show');
    });
</script>

</body>


</html>
