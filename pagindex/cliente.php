<?php
require_once '../modelo/cliente.entidad.php';
require_once '../modelo/cliente.model.php';
require_once '../modelo/tipoDocumento.entidad.php';
require_once '../modelo/tipoDocumento.model.php';
require_once '../modelo/municipio.entidad.php';
require_once '../modelo/municipio.model.php';
// Logica de negocio
$alm = new Cliente();
$model = new ClienteModel();
if(isset($_REQUEST['action']))
{
switch($_REQUEST['action'])
{
case 'actualizar':
$alm->__SET('idcliente',                   $_REQUEST['idcliente']);
$alm->__SET('tipo_documento_idtipo_documento',               $_REQUEST['tipoDocumento']);
$alm->__SET('numero_documento',            $_REQUEST['numero_documento']);
$alm->__SET('razon_social',                $_REQUEST['razon_social']);
$alm->__SET('persona_contacto',            $_REQUEST['persona_contacto']);
$alm->__SET('telefono',                    $_REQUEST['telefono']);
$alm->__SET('correo',                      $_REQUEST['correo']);
$alm->__SET('direccion',                   $_REQUEST['direccion']);
$alm->__SET('idmunicipio',                   $_REQUEST['idmunicipio']);
$model->Actualizar($alm);
header('Location: cliente.php');
break;
case 'registrar':
$alm->__SET('numero_documento',             $_REQUEST['numero_documento']);
$alm->__SET('tipoDocumento',                $_REQUEST['tipoDocumento']);
$alm->__SET('razon_social',                 $_REQUEST['razon_social']);
$alm->__SET('persona_contacto',             $_REQUEST['persona_contacto']);
$alm->__SET('telefono',                     $_REQUEST['telefono']);
$alm->__SET('correo',                       $_REQUEST['correo']);
$alm->__SET('direccion',                    $_REQUEST['direccion']);
$alm->__SET('idmunicipio',                     $_REQUEST['idmunicipio']);
 $model->Registrar($alm);
header('Location: cliente.php');
break;
case 'eliminar':
$model->Eliminar($_REQUEST['idcliente']);
header('Location: cliente.php');
break;
case 'editar':
$alm = $model->Obtener($_REQUEST['idcliente']);
break;
}
}
?>
<!DOCTYPE html>
<html lang="es">
<head>
 <h1>FORMULARIO DE ENTRADA...</h1><h1>CLIENTES</h1><br><br>
<title>Anexsoft</title>
 <link rel="stylesheet" href="http://yui.yahooapis.com/pure/0.5.0/pure-min.css">
</head>
 <body style="padding:15px;">

<div class="pure-g">
 <div class="pure-u-1-12">

 <form action="?action=<?php echo $alm->idcliente > 0 ? 'actualizar' : 'registrar'; ?>" method="post" class="pure-form pure-formstacked"
style="margin-bottom:30px;">
 <input type="hidden" name="idcliente" value="<?php echo $alm->__GET('idcliente'); ?>" />

 <table style="width:500px;">
 <tr>
    <th style="text-align:left;">Tipo Documento</th>
    <td>
        <select name="tipoDocumento" style="width:100%;">
            <option value="0">--Seleccione--</option>
                <?php
                $tp = new TipoDocumentoModel();
                foreach($tp->Listar() as $t):
                ?>
                <option value="<?php echo $t->__GET('idtipo_documento') ?>"
                    <?php echo $t->__GET('idtipo_documento') == $alm->__GET('tipoDocumento') ? 'selected' : ''?>>
                    <?php echo $t->__GET('descripcion') ?></option>
                <?php endforeach; ?>



                </select>
    </td>
 </tr>
 <tr>
 <th style="text-align:left;">Numero Documento</th>
 <td><input type="text" name="numero_documento" placeholder=" Documento Cliente" required="" value="<?php echo 
 $alm->__GET('numero_documento'); ?>" style="width:100%;" /></td>
 </tr>
 <tr>
 <th style="text-align:left;">Razon Social</th>
 <td><input type="text" name="razon_social" placeholder=" Razon Social" required="" value="<?php echo 
 $alm->__GET('razon_social'); ?>" style="width:100%;" /></td>
 </tr>
 <tr>
 <th style="text-align:left;">Persona Contacto</th>
 <td><input type="text" name="persona_contacto" placeholder=" Contacto" required="" value="<?php echo 
 $alm->__GET('persona_contacto'); ?>" style="width:100%;" /></td>
 </tr>
 <tr>
 <th style="text-align:left;">telefono</th>
 <td><input type="number" name="telefono" placeholder=" Telefono Cliente" required="" value="<?php echo 
 $alm->__GET('telefono'); ?>" style="width:100%;" /></td>
 </tr>
 <tr>
 <th style="text-align:left;">Correo</th>
 <td><input type="email" placeholder="Mail Cliente" required="" name=" correo" value="<?php echo 
 $alm->__GET('correo'); ?>" style="width:100%;" /></td>
 </tr>
 <tr>
 <th style="text-align:left;">Direccion</th>
 <td><input type="text" name="direccion" placeholder=" Direccion Cliente" required="" value="<?php echo 
 $alm->__GET('direccion'); ?>" style="width:100%;" /></td>
 </tr>
 <tr>
    <th style="text-align:left;">Cuidad</th>
    <td>
        <select name="idmunicipio" style="width:100%;">
            <option value="0">--Seleccione--</option>
                <?php
                $mo = new MunicipioModel();
                foreach($mo->Listar() as $m):
                ?>
                <option value="<?php echo $m->__GET('idmunicipio') ?>"
                <?php echo $m->__GET('idmunicipio') == $alm->__GET(
                'idmunicipio') ? 'selected' : ''?>>
                <?php echo $m->__GET('nombre_municipio') ?></option>
                <?php endforeach; ?>
                </select>
    </td>
 </tr>
 <tr>
 <td colspan="2">
 <button type="submit" class="pure-button pure-button-primary">Guardar</button>
 </td>
 </tr>
 </table>
 </form>
 <table class="pure-table pure-table-horizontal">
 <thead>
 <tr>
 <th style="text-align:left;">Tipo Documento</th>
 <th style="text-align:left;">Numero Documento</th> 
 <th style="text-align:left;">Razon Social</th>
 <th style="text-align:left;">Persona COntacto</th>
 <th style="text-align:left;">Telefono</th>
 <th style="text-align:left;">Mail</th>
 <th style="text-align:left;">Direccion</th>
 <th style="text-align:left;">Ciudad</th>
 <th></th>
 <th></th>
 </tr>
 </thead>
 <?php foreach($model->Listar() as $r): ?>
 <tr>
  <td><?php echo $r->__GET('tipoDocumento'); ?></td>
 <td><?php echo $r->__GET('numero_documento'); ?></td>
 <td><?php echo $r->__GET('razon_social'); ?></td>
 <td><?php echo $r->__GET('persona_contacto'); ?></td>
 <td><?php echo $r->__GET('telefono'); ?></td>
 <td><?php echo $r->__GET('correo'); ?></td>
 <td><?php echo $r->__GET('direccion'); ?></td>
 <td><?php echo $r->__GET('idmunicipio'); ?></td>
 <td>
 <a href="?action=editar&idcliente=<?php echo $r->idcliente; ?>">Editar</a>
 </td>
 <td>
 <a href="?action=eliminar&idcliente=<?php echo $r->idcliente; ?>">Eliminar</a>
 </td>
 </tr>
 <?php endforeach; ?>
 </table>

 </div>
 </div>

</div> <!-- fin editable -->



  <hr>
    <footer> <!-- inicio pie de pagina -->
        <div class="container"> 
          <div calss="row">
            <div class="col-xs-12">
                <div class="text-center">
            <font  class="pull-center" size="6" style="color: white">Elaborado por: <font style="color: red;"> 4SW</font>   Encuentranos en: </font>           
               <a  href="https://twitter.com/"> <img src="imagenes/twitter.png" width="50"></a >
               <a href="https://www.facebook.com/"> <img src="imagenes/facebook.png" width="50"></a >
               <a href="https://www.instagram.com/"> <img src="imagenes/instagram.png" width="50"></a >
               </div>
             </div>
         </div>      
        </div>
    </footer> <!-- fin pie de pagina -->


    <!-- jQuery -->
    <script src="bootstrap/js/jquery.min.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="bootstrap/js/bootstrap.min.js"></script>

    <!-- Metis Menu Plugin JavaScript -->
    <script src="bootstrap/css/metisMenu.min.js"></script>

    <script src=".script/bootstrap.min.js"></script>
    <!-- Metis Menu Plugin JavaScript -->
    <script src=".script/jquery.metisMenu.js"></script>
    <!-- DataTables JavaScript -->
    <script src="script/jquery.dataTables.js"></script>
    <script src="script/dataTables.bootstrap.js"></script>
    <!-- Morris Charts JavaScript -->
    <script src="script/raphael-2.1.0.min.js"></script>
    <script src="script/morris.js"></script>
    <!-- Morris Charts JavaScript -->
    <script src="script/Chart.js"></script>
    <script src="script/custom.js"></script>
    <script>
        $(document).ready(function () {
            $('#dataTables-example').dataTable();
        });
    </script>

    <script language="JavaScript"> 
        function mueveReloj(){ 
            momentoActual = new Date() 
            hora = momentoActual.getHours() 
            minuto = momentoActual.getMinutes() 
            segundo = momentoActual.getSeconds() 

            str_segundo = new String (segundo) 
            if (str_segundo.length == 1) 
                segundo = "0" + segundo 

            str_minuto = new String (minuto) 
            if (str_minuto.length == 1) 
                minuto = "0" + minuto 

            str_hora = new String (hora) 
            if (str_hora.length == 1) 
                hora = "0" + hora 

            horaImprimible = hora + " : " + minuto + " : " + segundo 

            document.form_reloj.reloj.value = horaImprimible 

            setTimeout("mueveReloj()",1000) 
        } 
    </script> 

    <!-- Morris Charts JavaScript -->
   <!--   <script src="../vendor/raphael/raphael.min.js"></script>
    <script src="../vendor/morrisjs/morris.min.js"></script>
    <script src="../data/morris-data.js"></script>
    <script src="./SisControl 3.0_files/jquery.metisMenu.js.descarga"></script> -->

    <!-- Custom Theme JavaScript -->
    <script src="bootstrap/js/sb-admin-2.js"></script>


    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->


<script type="text/javascript">
    $(document).ready(function(){
        $('#test_modal').modal('show');
    });
</script>

</body>


</html>
